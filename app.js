const changeIcon = (event) => {
  if (
    event.target.classList.contains("fa-eye") &&
    event.target.previousElementSibling.type === "password"
  ) {
    event.target.classList.remove("fa-eye");
    event.target.classList.add("fa-eye-slash");
    event.target.previousElementSibling.type = "text";
  } else if (
    event.target.classList.contains("fa-eye-slash") &&
    event.target.previousElementSibling.type === "text"
  ) {
    event.target.classList.remove("fa-eye-slash");
    event.target.classList.add("fa-eye");
    event.target.previousElementSibling.type = "password";
  }
};
const iconMain = () => {
  const icon = document.querySelector("i[data-icon=main]");
  icon.addEventListener("click", changeIcon);
  const icon2 = document.querySelector("i[data-icon=confirm]");
  icon2.addEventListener("click", changeIcon);
  const btn = document.querySelector(".btn");
  btn.addEventListener("click", CompareInputs);
};
const CompareInputs = () => {
  const valInput = document.querySelector("input[data-password=main]");
  const valInput2 = document.querySelector("input[data-password=confirm]");
  if (valInput.value !== valInput2.value) {
    alert("Потрібно ввести однакові значення");
  } else if (valInput.value === valInput2.value) {
    alert("You are welcome");
  }
};
document.addEventListener("DOMContentLoaded", () => {
  iconMain();
});
